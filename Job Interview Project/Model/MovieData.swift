//
//  MovieData.swift
//  Job Interview Project
//
//  Created by Ondrej Žiak on 22/04/2018.
//  Copyright © 2018 Ondrej Žiak. All rights reserved.
//

class MovieData {
    
    var movieID: String = ""
    var movieTitle: String = ""
    var movieGenres: [String] = []
    var posterURL: String = ""
    var movieReleaseDate: String = ""
    var movieOverview: String = ""
}
