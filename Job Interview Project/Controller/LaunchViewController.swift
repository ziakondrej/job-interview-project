//
//  LaunchViewController.swift
//  Job Interview Project
//
//  Created by Ondrej Žiak on 30/04/2018.
//  Copyright © 2018 Ondrej Žiak. All rights reserved.
//

import UIKit

class LaunchViewController: UIViewController {
    
    let network: NetworkManager = NetworkManager.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // If the network is unreachable show the offline view
        NetworkManager.isUnreachable { _ in
            self.goToOfflineView()
        }
        
        // If the network is reachable show the main view
        NetworkManager.isReachable { _ in
            self.goToMovieCatalogueView()
        }
    }
    
    private func goToOfflineView() -> Void {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "NetworkUnavailable", sender: self)
        }
    }
    
    private func goToMovieCatalogueView() -> Void {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "MainController", sender: self)
        }
    }
}
