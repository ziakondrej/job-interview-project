//
//  MovieDetailViewController.swift
//  Job Interview Project
//
//  Created by Ondrej Žiak on 22/04/2018.
//  Copyright © 2018 Ondrej Žiak. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import XCDYouTubeKit
import AVKit
import AVFoundation

class MovieDetailViewController: UIViewController {

    // Constants & variables
    let movieDetailsURL: String = "https://api.themoviedb.org/3/movie/"
    let movieVideoURL: String = "https://api.themoviedb.org/3/movie/"
    let apiKey: String = "?api_key=146c974f062c6fbc9feb40d71b7f69d2"
    let posterURL: String = "https://image.tmdb.org/t/p/w342/"
    
    @IBOutlet weak var detailImageView: UIImageView!
    @IBOutlet weak var movieTitleLabel: UILabel!
    @IBOutlet weak var genresLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    @IBOutlet var fullView: UIView!
    
    var movieData: MovieData = MovieData()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let fullURL: String = movieDetailsURL + movieData.movieID + apiKey

        getMovieDetailData(url: fullURL)
        
    }
    
    @objc func updateUI() {
        DispatchQueue.main.async(execute: {
            self.movieTitleLabel.text = self.movieData.movieTitle
            self.detailImageView.load(url: URL(string: self.movieData.posterURL)!)
            
            self.genresLabel.text = self.movieData.movieGenres[0]
            if self.movieData.movieGenres.count > 1 {
                for i in 1...(self.movieData.movieGenres.count - 1) {
                    self.genresLabel.text = self.genresLabel.text! + ", " + self.movieData.movieGenres[i]
                }
            }
            
            self.dateLabel.text = self.movieData.movieReleaseDate
            self.overviewLabel.text = self.movieData.movieOverview
        })
    }
    
    // Without this func movieTitleLabel.text disappears after rotation between landspace and portrait
    // Unknown reasons why
    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        updateUI()
    }
    
    //MARK: - networking
    func getMovieDetailData (url : String) {
        Alamofire.request(url, method: .get).responseJSON {
            response in
            if response.result.isSuccess {
                let dataJSON : JSON = JSON(response.result.value!)
                self.fillData(json: dataJSON)
            } else {
                print("Error \(response.result.error.debugDescription)")
            }
        }
    }
    
    func getMovieVideoData(url: String) {
        Alamofire.request(url, method: .get).responseJSON {
            response in
            if response.result.isSuccess {
                let dataJSON : JSON = JSON(response.result.value!)
                self.getTrailerKey(json: dataJSON)
            } else {
                print("Error \(response.result.error.debugDescription)")
            }
        }
    }
    
    //MARK: - JSON parsing
    func fillData(json : JSON) {
        if json["original_title"].string != nil {
            
            movieData.posterURL = posterURL + json["backdrop_path"].stringValue
            movieData.movieTitle = json["original_title"].stringValue
            
            let genresCount = json["genres"].count
            for i in 0...genresCount-1 {
                movieData.movieGenres.append(json["genres"][i]["name"].stringValue)
            }
            
            movieData.movieReleaseDate = json["release_date"].stringValue
            movieData.movieOverview = json["overview"].stringValue
            
            updateUI()
        }
        else {
            print("Error, couldn't get the detail data")
        }
    }
    
    func getTrailerKey(json: JSON) {
        if json["results"][0]["key"].string != nil {
            
            var index: Int = 0
            while (index <= json["results"].count) {
                if (json["results"][index]["type"].stringValue != "Trailer") {
                    index = index + 1
                } else {
                    break
                }
            }
            
            if (index <= json["results"].count) {
                playTrailer(key: json["results"][index]["key"].stringValue)
            } else {
                print("Trailer unavailable!")
            }
        } else {
            print("Error, couldn't get the trailer data")
        }
    }
    
    //MARK: - trailer player controls
    let playerViewController = AVPlayerViewController()
    
    struct YouTubeVideoQuality {
        static let hd720 = NSNumber(value: XCDYouTubeVideoQuality.HD720.rawValue)
        static let medium360 = NSNumber(value: XCDYouTubeVideoQuality.medium360.rawValue)
        static let small240 = NSNumber(value: XCDYouTubeVideoQuality.small240.rawValue)
    }
    
    @IBAction func trailerButtonPressed(_ sender: Any) {
        getMovieVideoData(url: movieVideoURL + movieData.movieID + "/videos" + apiKey)
    }
    
    func playTrailer(key: String?) {
        self.present(playerViewController, animated: true, completion: nil)
        
        XCDYouTubeClient.default().getVideoWithIdentifier(key) { [weak playerViewController] (video: XCDYouTubeVideo?, error: Error?) in
            if let streamURLs = video?.streamURLs,
                let streamURL = (streamURLs[XCDYouTubeVideoQualityHTTPLiveStreaming]
                    ?? streamURLs[YouTubeVideoQuality.hd720]
                    ?? streamURLs[YouTubeVideoQuality.medium360]
                    ?? streamURLs[YouTubeVideoQuality.small240]) {
                playerViewController?.player = AVPlayer(url: streamURL)
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.playerDidFinishPlaying(notification:)),
                                               name: Notification.Name.AVPlayerItemDidPlayToEndTime,
                                               object: nil)
        self.playerViewController.addObserver(self,
                                              forKeyPath: #keyPath(MovieDetailViewController.view.frame),
                                              options: .new,
                                              context: nil)
    }
    
    // Notification if player finnished playing
    @objc func playerDidFinishPlaying(notification: Notification) {
        playerViewController.dismiss(animated: true, completion: {self.updateUI()})
    }
    
    // Notification if user press Done button
    override func observeValue(forKeyPath keyPath: String?,
                               of object: Any?,
                               change: [NSKeyValueChangeKey : Any]?,
                               context: UnsafeMutableRawPointer?) {
        if (keyPath ==  #keyPath(MovieDetailViewController.view.frame)){
            if playerViewController.player?.rate == 0 {
                doneClicked()
            }
        }
    }

    // Update UI because faulty movie title label
    func doneClicked(){
        Timer.scheduledTimer(timeInterval: TimeInterval(0.1), target: self, selector: #selector(self.updateUI), userInfo: nil, repeats: false)
    }
}
