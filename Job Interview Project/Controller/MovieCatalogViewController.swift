//
//  MovieCatalogViewController.swift
//  Job Interview Project
//
//  Created by Ondrej Žiak on 22/04/2018.
//  Copyright © 2018 Ondrej Žiak. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class MovieCatalogViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating {
    
    // Constants & variables
    let polularMoviesURL: String = "https://api.themoviedb.org/3/movie/popular?api_key=146c974f062c6fbc9feb40d71b7f69d2"
    let pupularMoviesImageURL: String = "https://image.tmdb.org/t/p/w342/"
    let searchController = UISearchController(searchResultsController: nil)
    let network = NetworkManager.sharedInstance
    
    @IBOutlet weak var movieCatalogTableView: UITableView!
    
    var elements: [MovieData] = [MovieData]()
    var filteredMovies: [MovieData] = [MovieData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.hidesBackButton = true
        
        movieCatalogTableView.delegate = self
        movieCatalogTableView.dataSource = self
        
        getMovieData(url: polularMoviesURL)
        
        // Setup the Search Controller
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Popular Movies"
        navigationItem.searchController = searchController
        definesPresentationContext = true
        
        // If the network is unreachable show the main offline
        network.reachability.whenUnreachable = { reachability in
            self.goToOfflineView()
        }
    }
    
    //MARK: - segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if  segue.identifier == "goToMovieDetail",
            let destinationVC = segue.destination as? MovieDetailViewController,
            let index = movieCatalogTableView.indexPathForSelectedRow?.row as Int?
        {
            if isFiltering() {
                destinationVC.movieData.movieID = filteredMovies[index].movieID
            } else {
                destinationVC.movieData.movieID = elements[index].movieID
            }
        }
    }
    
    private func goToOfflineView() -> Void {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "NetworkUnavailable", sender: self)
        }
    }
    
    //MARK: - custom cell
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering() {
            return filteredMovies.count
        }
        
        return elements.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = movieCatalogTableView.dequeueReusableCell(withIdentifier: "movieCell", for: indexPath) as! MovieTableViewCell
        let movie: MovieData
        
        if isFiltering() {
            movie = filteredMovies[indexPath.row]
        } else {
            movie = elements[indexPath.row]
        }
        
        cell.movieName.text = movie.movieTitle
        cell.cellImageView.load(url: URL(string: movie.posterURL)!)

        return cell
    }
    
    //MARK: - networking
    func getMovieData (url : String) {
        Alamofire.request(url, method: .get).responseJSON {
            response in
            if response.result.isSuccess {                
                let dataJSON : JSON = JSON(response.result.value!)
                self.fillData(json: dataJSON)
                
            } else {
                print("Error \(response.result.error.debugDescription)")
            }
        }
    }
    
    //MARK: - JSON parsing
    func fillData(json : JSON) {
        if json["results"][0]["original_title"].string != nil {
            
            for i in 0...19 {
                let data : MovieData = MovieData()
                data.movieTitle = json["results"][i]["original_title"].stringValue
                //poster path seems like wrong information, chose to use backdrop_path instead
                data.posterURL = pupularMoviesImageURL + json["results"][i]["backdrop_path"].stringValue
                data.movieID = json["results"][i]["id"].stringValue
                elements.append(data)
            }
            
            updateUIData()
        }
        else {
            print("Error, couldnt get the data")
        }
    }
    
    //MARK: - UI update
    func updateUIData() {
        movieCatalogTableView.reloadData()
    }
    
    // MARK: - UISearchResultsUpdating Delegate
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!)
    }
    
    func searchBarIsEmpty() -> Bool {
        // Returns true if the text is empty or nil
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        filteredMovies = elements.filter({( movie : MovieData) -> Bool in
            return movie.movieTitle.lowercased().contains(searchText.lowercased())
        })
        
        updateUIData()
    }
    
    func isFiltering() -> Bool {
        return searchController.isActive && !searchBarIsEmpty()
    }
}
